import random # Henter inn random-biblioteket

tall = random.randint(1,100) # tall mellom 1 og 100
gjett = -1

while (tall!=gjett): #så lenge man gjetter feil
    gjett = int(input("Gjett tall mellom 1 og 100: "))
    if (gjett>tall):
        print("Psst: Litt for stort!")
    elif (gjett<tall):
        print("Psst: Litt for smått!")
 
print("RIKTIG!!!! Hvordan klarte du det????")
