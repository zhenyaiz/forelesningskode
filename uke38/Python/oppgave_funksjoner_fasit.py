# Del 1:
# Lag en funksjon differanse(tall1, tall2)
# Den skal _returnere_ differansen mellom tallene,
# absoluttverdien
# Inntil videre kaller vi bare denne ved at du skriver ut
# resultatet av et kall til differanse() med to valgfrie tall
def differanse(tall1, tall2):
    # La oss gjøre det enkelt
    return abs(tall1 - tall2)


# Del 2:
# Lag en funksjon tilfeldig_tall som spør brukeren om
# maksgrensen. Funksjonen skal returnere et tilfeldig tall
# mellom 0 og dette tallet.
import random
def tilfeldig_tall():
    tall = int(input('maks_tall: '))
    return random.randint(0, tall)


# Del 3
# Lag funksjonen main()
# Den skal hente inn to tilfeldige tall med
# maksgrense gitt over. Så skal den skrive
# ut tallene og absoluttverdien av differansen.
# kjør så main()
def main():
    tall1 = tilfeldig_tall()
    tall2 = tilfeldig_tall()
    print(tall1, tall2, differanse(tall1, tall2))

main()