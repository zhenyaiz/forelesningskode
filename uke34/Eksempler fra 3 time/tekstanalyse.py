# Oversikt over ordbruk i en tekst
# tekst ligger i "Enkel tekst.txt"
def åpne_fil(navn):
    f = open(navn,'r')
    tmp_tekst = f.read()
    f.close()
    return tmp_tekst

def lag_liste(tmp_tekst):
    tegn = ['-','–','   ','  ','.',',',':','!','?']
    for el in tegn:
        tmp_tekst = tmp_tekst.replace(el,'')
    return tmp_tekst

def lag_dikt(tmp_dikt, tmp_liste):
    for el in tmp_liste:
        tmp_dikt[el] = tmp_dikt.get(el,0)+1
    return tmp_dikt

def skriv_dikt():
    for el in dikt:
        print(f'{el:<20s}: {dikt[el]}')
        
def skriv_mengde():
    mengde = set(liste)
    for el in mengde:
        print(el)

innhold = åpne_fil('Enkel tekst.txt')
innhold = innhold.lower()
liste = lag_liste(innhold).split()
dikt = {}
dikt = lag_dikt(dikt, liste)
skriv_dikt()
#skriv_mengde()    
