# Diverse printhjelp
##
##print("*" * 20) #For å splitte opp litt...
##print("Hei\npå\ndeg\n.")
##print("*" * 20)
##print("Hei\tpå\tdeg\t.")


##print('Denne','setningen','er','passe','lang.',sep='?')
##print("Linje1", end='')
##print("Linje2")

tall = 2/7
print('før',format(tall,'.5f'),'etterpåtekst')
print(format(tall,'.2f'),'etterpåtekst')
print(format(tall,'10.2f'),'etterpåtekst')
print(format(tall,'100.7f'),'etterpåtekst')
